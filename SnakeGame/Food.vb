﻿Public Class Food
    Private FOODCOLOUR As Color = Color.Black

    Private food As PictureBox = New PictureBox

#Region "public properties"
    Public Property foodObject As PictureBox
        Get
            Return food
        End Get
        Set(value As PictureBox)
            food = value
        End Set
    End Property
#End Region

    Public Sub CreateFood()
        'Create the food picture box for the snake to find.
        With food
            .Width = rectDimensions.width
            .Height = rectDimensions.height
            .BackColor = FOODCOLOUR
            GameFeatures.GenerateRandomLocation(food)
        End With

    End Sub

    Public Sub RemoveFood()
        'Dispose of the food picture box.
        food.Dispose()
    End Sub
End Class
