﻿Public Class Snake
    Private SNAKECOLOUR As Color = Color.Green

    Private snakeBody(1000) As PictureBox
    Private snakeLength As Integer = 0

    Private randomId As New Random

    Private xValue As Integer = 0
    Private yValue As Integer = 0

    Private xUpdate As Integer = 0
    Private yUpdate As Integer = 0


#Region "public properties"
    Public Property snakeSegment As PictureBox
        Get
            Return snakeBody(snakeLength)
        End Get
        Set(value As PictureBox)
            snakeBody(snakeLength) = value
        End Set
    End Property

    Public Property xIncrement As Integer
        Get
            Return xUpdate
        End Get
        Set(value As Integer)
            xUpdate = value
        End Set
    End Property

    Public Property yIncrement As Integer
        Get
            Return yUpdate
        End Get
        Set(value As Integer)
            yUpdate = value
        End Set
    End Property
#End Region

#Region "boundry restrictions"
    Public Function HitRestriction() As ResponseType
        'Determine if snake has hit a collission object.
        Dim msgResponse As New ResponseType
        Dim formPicBox As PictureBox = GameBoard.PictureBox1

        If HitBoundry(formPicBox) Then
            msgResponse.ResponseError = True
            msgResponse.ResponseMessage = "Game Over: Hit boundry restriction."
        ElseIf HitSelf() Then
            msgResponse.ResponseError = True
            msgResponse.ResponseMessage = "Game Over: Colission with self."
        End If

        Return msgResponse

    End Function

    Private Function HitBoundry(ByVal formPicBox As PictureBox) As Boolean
        'Determine if snake has hit the limits of the picture box.
        With snakeBody(0)
            If .Top < formPicBox.Top Then
                Return True
            ElseIf .Bottom > formPicBox.Bottom Then
                Return True
            ElseIf .Left < formPicBox.Left Then
                Return True
            ElseIf .Right > formPicBox.Right Then
                Return True
            Else
                Return False
            End If
        End With
    End Function

    Private Function HitSelf() As Boolean
        'Determine if the snake has hit one of its own segments.
        For count = 1 To snakeLength
            If snakeBody(0).Bounds.IntersectsWith(snakeBody(count).Bounds) Then
                Return True
                Exit For
            End If
        Next
        Return False
    End Function
#End Region

    Public Sub CreateHead()
        'Create an initial segment to denote the snakes head.
        Call CreateSegment()
    End Sub

    Private Sub AddTail()
        'Add a new segment to the snake for increasing its length.
        snakeLength = snakeLength + 1
        Call CreateSegment()
    End Sub

    Public Function EatFood(ByVal foodbounds As Rectangle) As Boolean
        'If the snake is able to find a food picture box, add an additional segment.
        If snakeBody(0).Bounds.IntersectsWith(foodbounds) Then
            AddTail()
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub CreateSegment()
        'Create a new picture box in the location of the last created picture box.
        snakeBody(snakeLength) = New PictureBox
        With snakeBody(snakeLength)
            .Height = rectDimensions.height
            .Width = rectDimensions.width
            '.BackColor = SNAKECOLOUR
            .BackColor = Color.FromArgb(randomId.Next(0, 256), randomId.Next(0, 256), randomId.Next(0, 256))

            If snakeLength > 0 Then
                .Top = snakeBody(snakeLength - 1).Top
                .Left = snakeBody(snakeLength - 1).Left + .Width
            Else
                GameFeatures.GenerateRandomLocation(snakeBody(snakeLength))
            End If

        End With
    End Sub

    Public Sub UpdateSegment()
        'Move the snakes head forward by the x and y increment and give all other elements the preceeding elements location.
        If snakeLength > 0 Then
            For segmentId = snakeLength To 1 Step -1
                With snakeBody(segmentId)
                    .Left = snakeBody(segmentId - 1).Left
                    .Top = snakeBody(segmentId - 1).Top
                End With
            Next
        Else
            xValue = snakeBody(0).Left
            yValue = snakeBody(0).Top
        End If

        xValue += xIncrement
        yValue += yIncrement

        With snakeBody(0)
            .Left = xValue
            .Top = yValue
        End With

    End Sub

    Public Sub RemoveSnake()
        'Dispose of all picture boxes in the array.
        For count = 0 To snakeLength
            If Not snakeBody(count) Is Nothing Then
                snakeBody(count).Dispose()
            End If
        Next
        snakeLength = 0
    End Sub
End Class
