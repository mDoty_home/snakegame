﻿Public Class GameFeatures
    Private Shared randomId As New Random

    Public Shared Sub GenerateRandomLocation(ByRef itemPic As PictureBox)
        'Generate the random locations for game elements.
        Dim formPicBox As PictureBox = GameBoard.PictureBox1
        With itemPic
            .Top = randomId.Next(formPicBox.Top, formPicBox.Bottom - .Height)
            .Left = randomId.Next(formPicBox.Left, formPicBox.Right - .Width)
        End With
    End Sub
End Class
