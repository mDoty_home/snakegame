﻿Public Class GameBoard
    Dim snake As Snake
    Dim food As Food

    Dim lastDirection As Keys
    Dim directionPressed As Boolean = False

    Dim sleepCounter As Integer = gameRestrictions.defaultSleep

    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles startButton.Click
        'Operations when the user selects to start the game.
        startButton.Enabled = False
        If food Is Nothing Then
            AddFood()
        End If
        If snake Is Nothing Then
            CreateSnake()
        End If
        PlayGame()
    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, keyData As System.Windows.Forms.Keys) As Boolean
        'Override process command key with snake directions.
        If Not snake Is Nothing Then
            Select Case keyData
                Case Keys.Up
                    If lastDirection <> Keys.Up And lastDirection <> Keys.Down Then
                        lastDirection = keyData
                        snake.xIncrement = 0
                        snake.yIncrement = -1 * rectDimensions.height
                        directionPressed = True
                    End If
                Case Keys.Down
                    If lastDirection <> Keys.Up And lastDirection <> Keys.Down Then
                        lastDirection = keyData
                        snake.xIncrement = 0
                        snake.yIncrement = 1 * rectDimensions.height
                        directionPressed = True
                    End If
                Case Keys.Left
                    If lastDirection <> Keys.Left And lastDirection <> Keys.Right Then
                        lastDirection = keyData
                        snake.xIncrement = -1 * rectDimensions.width
                        snake.yIncrement = 0
                        directionPressed = True
                    End If
                Case Keys.Right
                    If lastDirection <> Keys.Left And lastDirection <> Keys.Right Then
                        lastDirection = keyData
                        snake.xIncrement = 1 * rectDimensions.width
                        snake.yIncrement = 0
                        directionPressed = True
                    End If
            End Select
        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function

    Private Sub PlayGame()
        'Progress the snake and check if boundry condition has been made or if food can be found.
        Dim hitDetection As ResponseType

        Do
            snake.UpdateSegment()
            hitDetection = snake.HitRestriction
            If hitDetection.ResponseError Then
                EndGame(hitDetection.ResponseMessage)
                Exit Do
            Else
                Threading.Thread.Sleep(sleepCounter)
                DetectFood()
            End If
            Application.DoEvents()
        Loop Until hitDetection.ResponseError

    End Sub


    Private Sub CreateSnake()
        snake = New Snake
        snake.CreateHead()
        FocusPictureBox(snake.snakeSegment)
    End Sub

    Private Sub AddFood()
        food = New Food
        food.CreateFood()
        FocusPictureBox(food.foodObject)
    End Sub

    Private Sub RemoveFood()
        food.RemoveFood()
        FocusPictureBox(snake.snakeSegment)
    End Sub

    Private Sub FocusPictureBox(ByRef picBoxObject As PictureBox)
        'After drawing component brint it to the front.
        Me.Controls.Add(picBoxObject)
        picBoxObject.BringToFront()
    End Sub

    Private Sub DetectFood()
        'If snake has detected the food, remove it and add new food elsewhere on gameboard.
        If snake.EatFood(food.foodObject.Bounds) Then
            RemoveFood()
            AddFood()
            'Possible to remove comment to force snake to move faster each time food eaten.
            'sleepCounter -= 10
        End If
    End Sub

    Private Sub EndGame(ByVal responseMsg As String)
        'Display error message and reset game parameters.
        MsgBox(responseMsg)
        ResetGame()
    End Sub

    Private Sub ResetGame()
        'Dispose of objects and reset global variables.
        snake.RemoveSnake()
        food.RemoveFood()
        snake = Nothing
        food = Nothing
        lastDirection = Keys.None
        sleepCounter = gameRestrictions.defaultSleep
        startButton.Enabled = True
    End Sub
End Class
