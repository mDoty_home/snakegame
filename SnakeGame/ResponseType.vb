﻿Public Class ResponseType
    Private rspMessage As String = ""
    Private rspError As Boolean = False

#Region "public properties"

    Public Property ResponseMessage As String
        Get
            Return rspMessage
        End Get
        Set(value As String)
            rspMessage = value
        End Set
    End Property

    Public Property ResponseError As Boolean
        Get
            Return rspError
        End Get
        Set(value As Boolean)
            rspError = value
        End Set
    End Property
#End Region
End Class
