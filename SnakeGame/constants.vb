﻿Module constants
    'Dimensions of the game shapes
    Enum rectDimensions As Integer
        width = 10
        height = 10
    End Enum

    'Generic game restrictions.
    Enum gameRestrictions As Integer
        defaultSleep = 100
    End Enum
End Module
